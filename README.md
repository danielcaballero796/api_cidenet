# Api Cidenet

### Clonar el proyecto, si se posee xampp o algun otro ambiente para despliegue local se descarga allí
```
git clone https://gitlab.com/danielcaballero796/api_cidenet.git
```
### Cambiar la conexion de la base de datos
```
app/config/config.php
```
### Se despliega la base de datos, se adjunta un archivo .sql 
```
api_cidenet.sql
```
### Una vez hecho esto tendremos disponible para el consumo los servicios 
```
- Information Api :
/

- Auth Routes :
/api/v1/auth

- Employee Routes : 
/api/v1/employees
/api/v1/employees
/api/v1/employees
/api/v1/employees

- Areas Routes : 
/api/v1/areas

- Countries Routes : 
/api/v1/countries

- TypesId Routes : 
/api/v1/typesId
```