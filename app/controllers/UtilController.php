<?php

declare(strict_types=1);

use Phalcon\Mvc\Controller;
require_once $config->application->libraryDir . 'php-jwt/src/JWT.php';
use Firebase\JWT\JWT;

class UtilController extends Controller
{

    /**
     * It takes a string, converts the string to uppercase and replaces the character "Ñ" with the character "N"
     * @param str The string to be cleaned.
     * @return txt The string cleaned 
     */
    public function cleanData($str)
    {
        setlocale(LC_ALL, "en_US.utf8"); 
        $str = iconv('UTF-8','ASCII//TRANSLIT',$str);
        $str = html_entity_decode($str);
        $str = $this->cleanNumbers($str);
        $str = $this->cleanSpecialCharacters($str);                
        return $str;
    }

    /**
     * It takes a string, clean all numbers
     * @param str The string to be cleaned.
     * @return txt The string cleaned 
     */
    private function cleanNumbers($str){
        return preg_replace('/[0-9]/', '', $str);
    }
    
    /**
     * It takes a string, clean some Special Characters
     * @param str The string to be cleaned.
     * @return txt The string cleaned 
     */
    private function cleanSpecialCharacters($str){
        return str_replace(
            array("\\", "¨", "º", "-", "~","#", "@", "|",
            "!", "\"","·", "$", "%", "&", "/","(", ")", "?",
            "'", "¡","¿", "[", "^", "`", "]","+", "}", "{", "_",
            "¨", "´",">", "< ", ";", ",", ":",".", "*", "="),
            '',
            $str);
    }

    /**
     * It takes a first name, last name, and country id and returns an email address
     * 
     * @param fn First name
     * @param fs First Surname
     * @param id_country 1 = Colombia, 2 = USA
     */
    public function makeMail($fn, $fs, $id_country)
    {
        $mail = "";

        $firstPart = str_replace(" ", "", strtolower($fn)) . '.' . str_replace(" ", "", strtolower($fs));
        $domain = ($id_country == 1) ? '@cidenet.com.co' : '@cidenet.com.us';

        $mail = $firstPart . $domain;

        $employee = Employee::findFirst([
            'conditions' => 'mail = ?1',
            'bind' => [
                1 => $mail,
            ]
        ]);

        if ($employee) {

            $id = Employee::maximum(
                [
                    'column' => 'id',
                ]
            );

            $mail = $firstPart . (intval($id) + 1) . $domain;
        }

        return $mail;
    }
    
    /**
     * It takes a first name, last name, and country id and returns an email address
     * 
     * @param fn First name
     * @param fs First Surname
     * @param id_country 1 = Colombia, 2 = USA
     * @param id Employee Id
     */
    public function makeMailUpdate($fn, $fs, $id_country, $id)
    {
        $mail = "";

        $firstPart = str_replace(" ", "", strtolower($fn)) . '.' . str_replace(" ", "", strtolower($fs));
        $domain = ($id_country == 1) ? '@cidenet.com.co' : '@cidenet.com.us';

        $mail = $firstPart . $domain;

        $employee = Employee::findFirst([
            'conditions' => 'mail = ?1 AND id <> ?2',
            'bind' => [
                1 => $mail,
                2 => $id,
            ]
        ]);

        if ($employee) {

            $id = Employee::maximum(
                [
                    'column' => 'id',
                ]
            );

            $mail = $firstPart . (intval($id) + 1) . $domain;
        }

        return $mail;
    }

    /**
     * It takes a date string and a format string, and returns true if the date string matches the
     * format string
     * 
     * @param date The date you want to validate.
     * @param format The format of the date.
     */
    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }


    /**
     * It checks if an employee exists in the database
     * @param id The identification of the employee to be checked.
     * @return flag boolean value.
     */
    public function employeeExist($id)
    {
        $flag = false;

        $employee = Employee::findFirst([
            'conditions' => 'identification = ?1',
            'bind' => [
                1 => $id,
            ]
        ]);

        if ($employee) {
            $flag = true;
        }

        return $flag;
    }

    /**
     * It checks if an employee have a valid token
     * @return flag boolean value.
     */
    public function isAuth(){
        $jwt = (empty(apache_request_headers()['Authorization'])) ? null : apache_request_headers()['Authorization'];

        if($jwt == null){
            return false;
        }

        $key = $this->config->application->prv_key;
        try {
            $decoded = JWT::decode($jwt, $key, array('HS256', 'HS384', 'HS512'));
            $time = time();         

            $employee = Employee::findFirst([
                'conditions' => 'mail = ?1 AND id = ?2 AND is_admin=1',
                'bind' => [
                    1 => $decoded->mail,
                    2 => $decoded->id
                ]
            ]);

            if ($decoded->dilav < $time || !$employee) {
                return false;
            } else {
                return true;
            }
        } catch (\Throwable $th) {
            return false;
        }
    }
}
