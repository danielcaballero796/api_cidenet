<?php

declare(strict_types=1);

use Phalcon\Http\Response;
use Phalcon\Http\Request;
use Phalcon\Mvc\Controller;

require_once $config->application->libraryDir . 'php-jwt/src/JWT.php';

use Firebase\JWT\JWT;

class AuthController extends Controller
{

    /**
     * This function receives a JSON object with the information of an employee for login in the app
     */
    public function loginAction()
    {
        // Disable View File Content
        $this->view->disable();

        // Getting a response instance
        // https://docs.phalcon.io/4.0/en/response
        $response = new Response();

        // Getting a request instance
        // https://docs.phalcon.io/4.0/en/request
        $request = new Request();

        // checking if the request comes by POST
        if ($request->isPost()) {

            // checking if the request comes by POST
            $data = $request->getJsonRawBody();
            $mail = $data->mail;

            // Check user exist in database table
            try {
                $employee = Employee::findFirst([
                    'conditions' => 'mail = ?1 AND is_admin=1',
                    'bind' => [
                        1 => $mail,
                    ]
                ]);

                if (!$employee) {
                    throw new Exception('You are not Admin user');
                }

                $key = $this->config->application->prv_key;

                $time = time(); //Fecha y hora actual en segundos
                $token = array(
                    'id' => $employee->id,
                    'mail' => $employee->mail,
                    'dilav' => $time + (3600) // Tiempo que inició el token
                );

                $alg = array('HS256', 'HS384', 'HS512');
                $jwt = JWT::encode($token, $key, $alg[random_int(0, 2)]); //Codificamos el Token

                // Set status code
                $response->setStatusCode(200, 'OK');

                // Set the response content
                $response->setJsonContent(["status" => true, "data" => $jwt]);
            } catch (\Throwable $th) {
                // Set status code
                $response->setStatusCode(400, 'Bad Request');

                // Set the response content
                $response->setJsonContent(["status" => false, "error" => "You can not login. " . $th->getMessage()]);
            }
        } else {

            // Set status code
            $response->setStatusCode(405, 'Method Not Allowed');

            // Set the response content
            // $response->setContent("Sorry, the page doesn't exist");
            $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);
        }

        // Send response to the client
        $response->send();
    }
}
