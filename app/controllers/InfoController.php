<?php

declare(strict_types=1);

use Phalcon\Http\Response;
use Phalcon\Http\Request;
use Phalcon\Mvc\Controller;

class InfoController extends Controller
{

    /**
     * It returns the information of the API.
     */
    public function getInfoAction()
    {
        // the view component is disabled avoiding unnecessary processing
        $this->view->disable();

        // https://docs.phalcon.io/4.0/en/response
        $response = new Response();

        // Getting a request instance
        // https://docs.phalcon.io/4.0/en/request
        $request = new Request();

        // checking if the request comes by GET
        if ($request->isGet()) {

            // Check user exist in database table
            try {
                // Set status code
                $response->setStatusCode(200, 'ok');

                $info = json_encode(array('api' => '1.0', 'author' => 'Daniel Caballero.', 'date' => '04/06/2022'));
                $info = json_decode($info);
                // Set the response content
                $response->setJsonContent(["status" => true, "data" => $info]);
            } catch (\Throwable $th) {
                // Set status code
                $response->setStatusCode(200, 'ok');
                // Set the response content
                $response->setJsonContent(["status" => true, "data" => []]);
            }
        } else {

            // Set status code
            $response->setStatusCode(405, 'Method Not Allowed');
            // Set the response content
            $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);
        }

        // Send response
        $response->send();
    }
}
