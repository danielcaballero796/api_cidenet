<?php

declare(strict_types=1);

use Phalcon\Http\Response;
use Phalcon\Http\Request;
use Phalcon\Mvc\Controller;

class CountryController extends Controller
{

    /**
     * It receives a GET request, it queries the database and returns the countries result in JSON format
     * @method GET
     */
    public function getCountriesAction()
    {
        // the view component is disabled avoiding unnecessary processing
        $this->view->disable();

        // https://docs.phalcon.io/4.0/en/response
        $response = new Response();

        // Getting a request instance
        // https://docs.phalcon.io/4.0/en/request
        $request = new Request();

        // checking if the request comes by GET
        if ($request->isGet()) {

            try {
                $areas = Country::find();

                // Set status code
                $response->setStatusCode(200, 'ok');

                // Set the response content
                $response->setJsonContent(["status" => true, "data" => $areas]);
            } catch (\Throwable $th) {
                // Set status code
                $response->setStatusCode(200, 'ok');
                // Set the response content
                $response->setJsonContent(["status" => true, "data" => [], "error" => $th->getMessage()]);
            }
        } else {

            // Set status code
            $response->setStatusCode(405, 'Method Not Allowed');
            // Set the response content
            $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);
        }

        // Send response
        $response->send();
    }
}
