<?php

declare(strict_types=1);

use Phalcon\Http\Response;
use Phalcon\Http\Request;
use Phalcon\Mvc\Controller;

require_once $config->application->controllersDir . 'UtilController.php';

class EmployeeController extends Controller
{

    /**
     * It receives a GET request, it queries the database and returns the employee result in JSON format
     * @method GET
     */
    public function getEmployeesAction()
    {
        // the view component is disabled avoiding unnecessary processing
        $this->view->disable();

        // https://docs.phalcon.io/4.0/en/response
        $response = new Response();

        // Getting a request instance
        // https://docs.phalcon.io/4.0/en/request
        $request = new Request();

        $util = new UtilController();
        if (!$util->isAuth()) {
            $response->setStatusCode(401, 'Unauthorized');
            $response->setJsonContent(["status" => false, "data" => "Token de sesion invalido"]);
        } else {

            // checking if the request comes by GET
            if ($request->isGet()) {

                try {

                    $employees = Employee::query()
                        ->columns('Employee.*, Area.*, Country.*, TypeIdentification.*')
                        ->where('is_admin <> 1')
                        ->innerJoin('Area', 'Employee.id_area = Area.id',)
                        ->innerJoin('Country', 'Employee.id_country = Country.id',)
                        ->innerJoin('TypeIdentification', 'Employee.id_type_id = TypeIdentification.id',)
                        ->execute();

                    $myEmployees = array();
                    for ($i = 0; $i < count($employees); $i++) {

                        $emp = json_encode([
                            "id" => $employees[$i]["employee"]->id,
                            "first_name" => $employees[$i]["employee"]->first_name,
                            "others_names" => $employees[$i]["employee"]->others_names,
                            "first_surname" => $employees[$i]["employee"]->first_surname,
                            "second_surname" => $employees[$i]["employee"]->second_surname,
                            "id_country" => $employees[$i]["employee"]->id_country,
                            "country" => $employees[$i]["country"]->name,
                            "id_type_id" => $employees[$i]["employee"]->id_type_id,
                            "type_identification" => $employees[$i]["typeIdentification"]->name,
                            "identification" => $employees[$i]["employee"]->identification,
                            "mail" => $employees[$i]["employee"]->mail,
                            "entry_date" => $employees[$i]["employee"]->entry_date,
                            "id_area" => $employees[$i]["employee"]->id_area,
                            "area" => $employees[$i]["area"]->name,
                            "state" => $employees[$i]["employee"]->state,
                            "register_date" => $employees[$i]["employee"]->register_date,
                            "updated_date" => $employees[$i]["employee"]->updated_date,
                            "is_admin" => $employees[$i]["employee"]->is_admin,
                        ]);

                        $emp = json_decode($emp);
                        array_push($myEmployees, $emp);
                    }

                    // Set status code
                    $response->setStatusCode(200, 'ok');

                    // Set the response content
                    $response->setJsonContent(["status" => true, "data" => $myEmployees]);
                } catch (\Throwable $th) {
                    // Set status code
                    $response->setStatusCode(200, 'ok');
                    // Set the response content
                    $response->setJsonContent(["status" => true, "data" => [], "error" => $th->getMessage()]);
                }
            } else {

                // Set status code
                $response->setStatusCode(405, 'Method Not Allowed');
                // Set the response content
                $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);
            }
        }

        // Send response
        $response->send();
    }

    /**
     * This function receives a JSON object with the information of an employee, validates it and
     * inserts it into the database
     */
    public function insertEmployeeAction()
    {
        // Disable View File Content
        $this->view->disable();

        // https://docs.phalcon.io/4.0/en/response
        $response = new Response();

        // Getting a request instance
        // https://docs.phalcon.io/4.0/en/request
        $request = new Request();

        $util = new UtilController();
        if (!$util->isAuth()) {
            $response->setStatusCode(401, 'Unauthorized');
            $response->setJsonContent(["status" => false, "data" => "Token de sesion invalido"]);
        } else {

            // checking if the request comes by POST
            if ($request->isPost()) {

                try {

                    if ($request->getrawBody() == "") {
                        throw new Exception('The body of the request does not be empty.');
                    }

                    $data = $request->getJsonRawBody();
                    $util = new UtilController();

                    /* get information and validate it */
                    $first_name = isset($data->first_name) ? strtoupper($util->cleanData($data->first_name)) : "";
                    $others_names = isset($data->others_names) ? strtoupper($util->cleanData($data->others_names)) : "";
                    $first_surname = isset($data->first_surname) ? strtoupper($util->cleanData($data->first_surname)) : "";
                    $second_surname = isset($data->second_surname) ? strtoupper($util->cleanData($data->second_surname)) : "";
                    $id_country = isset($data->id_country) ? $data->id_country : "-1";
                    $id_type_id = isset($data->id_type_id) ? $data->id_type_id : "-1";
                    $mail = $util->makeMail($first_name, $first_surname, $id_country);
                    $id_area = isset($data->id_area) ? $data->id_area : "-1";

                    if (
                        $first_name == "" || $others_names == "" || $first_surname == "" ||
                        $second_surname == "" || $id_country == "-1" || $id_type_id == ".1" ||
                        $mail == "" || $id_area == "-1"
                    ) {
                        throw new Exception('You must complete all fields.');
                    }

                    if (!ctype_alnum($data->identification)) {
                        throw new Exception('Not all characters in the identification are alphanumeric');
                    } else {
                        $identification = $data->identification;
                    }

                    if (!$util->validateDate($data->entry_date)) {
                        throw new Exception('Invalid Entry Date');
                    } else {
                        $entry_date = $data->entry_date;
                    }


                    if ($util->employeeExist($identification)) {
                        throw new Exception('There is already an employee with that identification number');
                    }

                    /* Create the model for insert in the db */
                    $employee = new Employee();
                    $employee->first_name = $first_name;
                    $employee->others_names = $others_names;
                    $employee->first_surname = $first_surname;
                    $employee->second_surname = $second_surname;
                    $employee->id_country = $id_country;
                    $employee->id_type_id = $id_type_id;
                    $employee->identification = $identification;
                    $employee->mail = $mail;
                    $employee->entry_date = $entry_date;
                    $employee->id_area = $id_area;

                    if ($employee->save() === false) {
                        $response->setStatusCode(500, 'Bad Request');

                        // Set the response content
                        $response->setJsonContent(["status" => false, "error" => "The employee could not be registered. "]);
                    } else {
                        $response->setStatusCode(200, 'ok');

                        // Set the response content
                        $response->setJsonContent(["status" => true, "data" => "The employee successfully registered"]);
                    }
                } catch (\Throwable $th) {
                    // Set status code
                    $response->setStatusCode(400, 'Bad Request');

                    // Set the response content
                    $response->setJsonContent(["status" => false, "error" => "The employee could not be registered. " . $th->getMessage()]);
                }
            } else {

                // Set status code
                $response->setStatusCode(405, 'Method Not Allowed');

                // Set the response content
                $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);
            }
        }


        // Send response
        $response->send();
    }

    /**
     * This function receives a JSON object with the information of an employee, validates it and
     * updates it into the database
     */
    public function updateEmployeeAction()
    {
        // the view component is disabled avoiding unnecessary processing
        $this->view->disable();

        // https://docs.phalcon.io/4.0/en/response
        $response = new Response();

        // Getting a request instance
        // https://docs.phalcon.io/4.0/en/request
        $request = new Request();

        $util = new UtilController();
        if (!$util->isAuth()) {
            $response->setStatusCode(401, 'Unauthorized');
            $response->setJsonContent(["status" => false, "data" => "Token de sesion invalido"]);
        } else {

            // checking if the request comes by PUT
            if ($request->isPut()) {

                try {

                    if ($request->getrawBody() == "") {
                        throw new Exception('The body of the request does not be empty.');
                    }

                    $data = $request->getJsonRawBody();
                    $util = new UtilController();

                    /* get information and validate it */
                    $id_employee = isset($data->id) ? $data->id : "";
                    $first_name = isset($data->first_name) ? strtoupper($util->cleanData($data->first_name)) : "";
                    $others_names = isset($data->others_names) ? strtoupper($util->cleanData($data->others_names)) : "";
                    $first_surname = isset($data->first_surname) ? strtoupper($util->cleanData($data->first_surname)) : "";
                    $second_surname = isset($data->second_surname) ? strtoupper($util->cleanData($data->second_surname)) : "";
                    $id_country = isset($data->id_country) ? $data->id_country : "-1";
                    $id_type_id = isset($data->id_type_id) ? $data->id_type_id : "-1";
                    $mail = $util->makeMailUpdate($first_name, $first_surname, $id_country, $id_employee);
                    $id_area = isset($data->id_area) ? $data->id_area : "-1";

                    if (
                        $id_employee == "" || $first_name == "" || $others_names == "" || $first_surname == "" ||
                        $second_surname == "" || $id_country == "-1" || $id_type_id == ".1" ||
                        $mail == "" || $id_area == "-1"
                    ) {
                        throw new Exception('You must complete all fields.');
                    }

                    if (!ctype_alnum($data->identification)) {
                        throw new Exception('Not all characters in the identification are alphanumeric');
                    } else {
                        $identification = $data->identification;
                    }

                    if (!$util->validateDate($data->entry_date)) {
                        throw new Exception('Invalid Entry Date');
                    } else {
                        $entry_date = $data->entry_date;
                    }

                    /* find the model for update in the db */
                    $employee = Employee::findFirstById($id_employee);

                    if (!$employee) {
                        throw new Exception('The employee does not exist in the database');
                    }

                    $employee->first_name = $first_name;
                    $employee->others_names = $others_names;
                    $employee->first_surname = $first_surname;
                    $employee->second_surname = $second_surname;
                    $employee->id_country = $id_country;
                    $employee->id_type_id = $id_type_id;
                    $employee->identification = $identification;
                    $employee->mail = $mail;
                    $employee->entry_date = $entry_date;
                    $employee->id_area = $id_area;
                    $employee->id = $id_employee;

                    date_default_timezone_set('America/Bogota');
                    $updatedDate = date('Y-m-d h:i:s a', time());
                    $employee->updated_date = $updatedDate;

                    if ($employee->update() === false) {
                        $response->setStatusCode(500, 'Bad Request');

                        // Set the response content
                        $response->setJsonContent(["status" => false, "error" => "The employee could not be updated. "]);
                    } else {
                        $response->setStatusCode(200, 'ok');

                        // Set the response content
                        $response->setJsonContent(["status" => true, "data" => "The employee successfully updated"]);
                    }
                } catch (\Throwable $th) {
                    // Set status code
                    $response->setStatusCode(400, 'Bad Request');

                    // Set the response content
                    $response->setJsonContent(["status" => false, "error" => "The employee could not be updated. " . $th->getMessage()]);
                }
            } else {

                // Set status code
                $response->setStatusCode(405, 'Method Not Allowed');

                // Set the response content
                $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);
            }
        }


        // Send response
        $response->send();
    }

    /**
     * This function receives a JSON object with the information of an employee, validates it and
     * updates it into the database
     */
    public function deleteEmployeeAction()
    {
        // the view component is disabled avoiding unnecessary processing
        $this->view->disable();

        // https://docs.phalcon.io/4.0/en/response
        $response = new Response();

        // Getting a request instance
        // https://docs.phalcon.io/4.0/en/request
        $request = new Request();

        $util = new UtilController();
        if (!$util->isAuth()) {
            $response->setStatusCode(401, 'Unauthorized');
            $response->setJsonContent(["status" => false, "data" => "Token de sesion invalido"]);
        } else {

            // checking if the request comes by DELETE
            if ($request->isDelete()) {

                try {
                    if ($request->getrawBody() == "") {
                        throw new Exception('The body of the request does not be empty.');
                    }

                    $data = $request->getJsonRawBody();
                    $id_employee = isset($data->id) ? $data->id : "";

                    if ($id_employee == "") {
                        throw new Exception('You must complete all fields.');
                    }

                    $employee = Employee::findFirst($id_employee);

                    if (!$employee) {
                        throw new Exception('The employee does not exist in the database');
                    }

                    if ($employee->delete() === false) {
                        $response->setStatusCode(500, 'Bad Request');

                        // Set the response content
                        $response->setJsonContent(["status" => false, "error" => "The employee could not be deleted. "]);
                    } else {
                        $response->setStatusCode(200, 'ok');

                        // Set the response content
                        $response->setJsonContent(["status" => true, "data" => "The employee successfully deleted"]);
                    }
                } catch (\Throwable $th) {
                    // Set status code
                    $response->setStatusCode(400, 'Bad Request');

                    // Set the response content
                    $response->setJsonContent(["status" => false, "error" => "The employee could not be updated. " . $th->getMessage()]);
                }
            } else {

                // Set status code
                $response->setStatusCode(405, 'Method Not Allowed');

                // Set the response content
                $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);
            }
        }


        // Send response
        $response->send();
    }
}
