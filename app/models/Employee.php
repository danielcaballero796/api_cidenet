<?php

class Employee extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $first_name;

    /**
     *
     * @var string
     */
    public $others_names;

    /**
     *
     * @var string
     */
    public $first_surname;

    /**
     *
     * @var string
     */
    public $second_surname;

    /**
     *
     * @var integer
     */
    public $id_country;

    /**
     *
     * @var integer
     */
    public $id_type_id;

    /**
     *
     * @var string
     */
    public $identification;

    /**
     *
     * @var string
     */
    public $mail;

    /**
     *
     * @var string
     */
    public $entry_date;

    /**
     *
     * @var integer
     */
    public $id_area;

    /**
     *
     * @var integer
     */
    public $state;

    /**
     *
     * @var string
     */
    public $register_date;

    /**
     *
     * @var string
     */
    public $updated_date;

    /**
     *
     * @var integer
     */
    public $is_admin;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("api_cidenet");
        $this->setSource("employee");
        $this->belongsTo('id_area', '\Area', 'id', ['alias' => 'Area']);
        $this->belongsTo('id_country', '\Country', 'id', ['alias' => 'Country']);
        $this->belongsTo('id_type_id', '\TypeIdentification', 'id', ['alias' => 'TypeIdentification']);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Employee[]|Employee|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null): \Phalcon\Mvc\Model\ResultsetInterface
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Employee|\Phalcon\Mvc\Model\ResultInterface|\Phalcon\Mvc\ModelInterface|null
     */
    public static function findFirst($parameters = null): ?\Phalcon\Mvc\ModelInterface
    {
        return parent::findFirst($parameters);
    }

}
