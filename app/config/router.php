<?php

$router = $di->getRouter();

// https://docs.phalcon.io/4.0/en/routing

/* Information Api */
$router->add("/", "info::getInfo", ["GET"]);

/* Auth Routes */
$router->add("/api/v1/auth", "auth::login", ["POST"]);

/* Employee Routes */
$router->add("/api/v1/employees", "employee::getEmployees", ["GET"]);
$router->add("/api/v1/employees", "employee::insertEmployee", ["POST"]);
$router->add("/api/v1/employees", "employee::updateEmployee", ["PUT"]);
$router->add("/api/v1/employees", "employee::deleteEmployee", ["DELETE"]);

/* Areas Routes */
$router->add("/api/v1/areas", "area::getAreas", ["GET"]);

/* Countries Routes */
$router->add("/api/v1/countries", "country::getCountries", ["GET"]);

/* TypesId Routes */
$router->add("/api/v1/typesId", "typeid::getTypesId", ["GET"]);

$router->handle($_SERVER['REQUEST_URI']);
