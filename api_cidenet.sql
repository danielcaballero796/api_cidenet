-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-06-2022 a las 06:32:35
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `api_cidenet`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla para gestionar las areas de la empresa';

--
-- RELACIONES PARA LA TABLA `area`:
--

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`id`, `name`) VALUES
(1, 'Administración'),
(2, 'Financiera'),
(3, 'Compras'),
(4, 'Infraestructura'),
(5, 'Operación'),
(6, 'Talento Humano'),
(7, 'Servicios Varios'),
(8, 'Desarrollo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla para gestionar los paises';

--
-- RELACIONES PARA LA TABLA `country`:
--

--
-- Volcado de datos para la tabla `country`
--

INSERT INTO `country` (`id`, `name`) VALUES
(1, 'Colombia'),
(2, 'Estados Unidos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `first_name` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `others_names` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `first_surname` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `second_surname` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_type_id` int(11) NOT NULL,
  `identification` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `mail` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `entry_date` date NOT NULL,
  `id_area` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT 1,
  `register_date` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_admin` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla para gestionar los empleados';

--
-- RELACIONES PARA LA TABLA `employee`:
--   `id_area`
--       `area` -> `id`
--   `id_country`
--       `country` -> `id`
--   `id_type_id`
--       `type_identification` -> `id`
--

--
-- Volcado de datos para la tabla `employee`
--

INSERT INTO `employee` (`id`, `first_name`, `others_names`, `first_surname`, `second_surname`, `id_country`, `id_type_id`, `identification`, `mail`, `entry_date`, `id_area`, `state`, `register_date`, `updated_date`, `is_admin`) VALUES
(1, 'DANIEL', 'JOSE', 'CABALLERO', 'SANCHEZ', 1, 1, '1111111111', 'daniel.caballero@cidenet.com.co', '2020-06-04', 1, 1, '2022-06-04 11:11:57', '2022-06-06 04:29:38', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `type_identification`
--

CREATE TABLE `type_identification` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='tabla para gestionar los tipos de identificaciones';

--
-- RELACIONES PARA LA TABLA `type_identification`:
--

--
-- Volcado de datos para la tabla `type_identification`
--

INSERT INTO `type_identification` (`id`, `name`) VALUES
(1, 'Cédula de Ciudadanía'),
(2, 'Cédula de Extranjería'),
(3, 'Pasaporte'),
(4, 'Permiso Especial');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_area` (`id_area`),
  ADD KEY `id_country` (`id_country`),
  ADD KEY `id_type_id` (`id_type_id`);

--
-- Indices de la tabla `type_identification`
--
ALTER TABLE `type_identification`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `type_identification`
--
ALTER TABLE `type_identification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `id_area` FOREIGN KEY (`id_area`) REFERENCES `area` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_country` FOREIGN KEY (`id_country`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_type_id` FOREIGN KEY (`id_type_id`) REFERENCES `type_identification` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
