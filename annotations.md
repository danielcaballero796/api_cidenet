# Api Cidenet

### se modifica la linea 44 de public/index para correrlo en xampp

```
echo $application->handle($_GET['_url'] ?? '/')->getContent();
```

### colocamos la configuracion de la base de datos
```
App/config/config.php
```

### ejecutamos el comando para mapear la bd si queremos la data colocamos la bandera --data
```
phalcon migration --action generate --data always
```

### creamos el modelo de empleado || el mapea los campos de la bd 
```
phalcon model Empleado
phalcon all-models //por si los queremos todos 
```

### creamos el controlador para el api
```
phalcon create-controller API
```

### modificamos el archivo de rutas para crear nuestras rutas personalizadas
```
App/config/router.php
```